package com.mypackage.henri.pubgi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import pro.lukasgorny.core.JPubg;
import pro.lukasgorny.dto.FilterCriteria;
import pro.lukasgorny.dto.Player;
import pro.lukasgorny.dto.Stat;
import pro.lukasgorny.enums.PUBGMode;
import pro.lukasgorny.enums.PUBGRegion;
import pro.lukasgorny.enums.PUBGSeason;
import pro.lukasgorny.enums.PUBGStat;
import pro.lukasgorny.factory.JPubgFactory;

public class PlayerDataActivity extends AppCompatActivity implements  SharedPreferences.OnSharedPreferenceChangeListener{
    private final String API_KEY =  "9efc63a0-90ca-4b10-a357-8f08fd24c4b7";
    ProgressBar pb;
    TextView playerNameTxt;
    ImageView avatarImg;
    TextView mostkillsTxt;
    TextView kdrTxt;
    TextView winsTxt;
    TextView killsTxt;

    TextView headshotTxt;
    TextView longshotTxt;
    TextView seasonDisplayTxt;
    TextView gamemodeTxt;

    Player player;
    FilterCriteria criteria;
    JPubg jPubg;
    String playerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_data);


        ActionBar actionBar = this.getSupportActionBar();

        // Set the action bar back button to look like an up button
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        pb = (ProgressBar)findViewById(R.id.playerActivityProgressBar);
        playerNameTxt = (TextView)findViewById(R.id.friendName);
        avatarImg = (ImageView)findViewById(R.id.friendAvatar);
        mostkillsTxt = (TextView)findViewById(R.id.playerMostKills);
        kdrTxt = (TextView)findViewById(R.id.playerKdr);
        winsTxt = (TextView)findViewById(R.id.playerMostWins);
        killsTxt = (TextView)findViewById(R.id.playerKills);

        headshotTxt = (TextView)findViewById(R.id.playerHeadshots);
        longshotTxt = (TextView)findViewById(R.id.longestShot);
        seasonDisplayTxt = (TextView)findViewById(R.id.seasonDisplayTxt);
        gamemodeTxt = (TextView)findViewById(R.id.gamemodeTxt);


        jPubg = JPubgFactory.getWrapper(API_KEY);
        criteria = new FilterCriteria();
        criteria.setSeason(PUBGSeason.PRE5_2017);
        applySettings();


        Intent i = getIntent();
        playerName = i.getStringExtra("name");

        new LoadPlayerDataTask().execute();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.main_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.settings:
                Intent settingsIntent = new Intent(PlayerDataActivity.this, SettingsActivity.class);
                startActivity(settingsIntent);
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


    }

    private void applySettings(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        preferences.registerOnSharedPreferenceChangeListener(this);

        String region = preferences.getString("pref_region", "EU");
        String gamemode = preferences.getString("pref_gamemodes", "Squad-Fpp");


            switch (region){
                case "EU":
                    criteria.setRegion(PUBGRegion.eu);
                    break;
                case "SEA":
                    criteria.setRegion(PUBGRegion.sea);
                    break;
                case "SA":
                    criteria.setRegion(PUBGRegion.sa);
                    break;
                case "NA":
                    criteria.setRegion(PUBGRegion.na);
                    break;
                case "KR/JP":
                    criteria.setRegion(PUBGRegion.krjp);
                    break;
                case "OC":
                    criteria.setRegion(PUBGRegion.oc);
                    break;
                case "AS":
                    criteria.setRegion(PUBGRegion.as);
                    break;
                default:
                    criteria.setRegion(PUBGRegion.eu);
            }


            switch (gamemode){
                case "Solo":
                    criteria.setMode(PUBGMode.solo);
                    break;
                case "Duo":
                    criteria.setMode(PUBGMode.duo);
                    break;
                case "Squad":
                    criteria.setMode(PUBGMode.squad);
                    break;
                case "Solo-Fpp":
                    criteria.setMode(PUBGMode.solo_fpp);
                    break;
                case "Duo-Fpp":
                    criteria.setMode(PUBGMode.duo_fpp);
                    break;
                case "Squad-Fpp":
                    criteria.setMode(PUBGMode.squad_fpp);
                    break;
                default:
                    criteria.setMode(PUBGMode.squad_fpp);
            }


    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {

    }




    class LoadPlayerDataTask extends AsyncTask<Void,Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {


            try{

                player = jPubg.getByNickname(playerName, criteria);

            }catch (Exception e){
                Toast.makeText(PlayerDataActivity.this, "Sorry, something went wrong", Toast.LENGTH_SHORT).show();

            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            pb.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            pb.setVisibility(View.INVISIBLE);

            try{

                Stat kdrStat = jPubg.getPlayerMatchStatByStatName(player, PUBGStat.KILL_DEATH_RATIO);
                Stat mostkillsStat = jPubg.getPlayerMatchStatByStatName(player, PUBGStat.ROUND_MOST_KILLS);
                Stat winsStat = jPubg.getPlayerMatchStatByStatName(player, PUBGStat.WINS);
                Stat longestKill = jPubg.getPlayerMatchStatByStatName(player, PUBGStat.LONGEST_KILL);

                Stat headshots = jPubg.getPlayerMatchStatByStatName(player, PUBGStat.HEADSHOT_KILLS);
                Stat kills = jPubg.getPlayerMatchStatByStatName(player, PUBGStat.KILLS);

                String[] modetxtR = criteria.getMode().toString().split("_");
                String modeTxt = modetxtR[0] + " " + modetxtR[1];

                seasonDisplayTxt.setText("CURRENT SEASON");
                gamemodeTxt.setText(modeTxt.toUpperCase());



                playerNameTxt.setText(player.getPlayerName());
                kdrTxt.setText(kdrStat.getDisplayValue());
                mostkillsTxt.setText(mostkillsStat.getDisplayValue());
                winsTxt.setText(winsStat.getDisplayValue());
                killsTxt.setText(kills.getDisplayValue());
                longshotTxt.setText(longestKill.getDisplayValue());

                headshotTxt.setText(headshots.getDisplayValue());



                if(player.getAvatar() != null && !player.getAvatar().isEmpty()){
                    Glide.with(PlayerDataActivity.this).load(player.getAvatar()).into(avatarImg);
                }
            }
            catch (Exception e){
                Toast.makeText(PlayerDataActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            }

        }



    }
}
