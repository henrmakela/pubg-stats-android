package com.mypackage.henri.pubgi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import pro.lukasgorny.core.JPubg;
import pro.lukasgorny.dto.FilterCriteria;
import pro.lukasgorny.dto.Player;
import pro.lukasgorny.dto.Stat;
import pro.lukasgorny.enums.PUBGMode;
import pro.lukasgorny.enums.PUBGRegion;
import pro.lukasgorny.enums.PUBGSeason;
import pro.lukasgorny.enums.PUBGStat;
import pro.lukasgorny.factory.JPubgFactory;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener{

    private final String API_KEY =  "9efc63a0-90ca-4b10-a357-8f08fd24c4b7";
    private static final String TAG = "MainActivity";

    ProgressBar pb;
    Player player;
    Button searchBtn;
    Button viewProfileBtn;
    EditText nickname;
    TextView header;
    TextView nicknameTxtV;
    TextView mostkillsTxtV;
    TextView kdrTxtV;
    TextView winsTxtV;
    TextView winsLabel;
    TextView kdrLabel;
    TextView mostkillsLabel;

    ImageView icon1;
    ImageView icon2;
    ImageView icon3;
    Intent profileIntent;


    ImageView avatarImg;
    JPubg jPubg;
    FilterCriteria criteria;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_player);

        jPubg = JPubgFactory.getWrapper(API_KEY);

        pb = (ProgressBar)findViewById(R.id.progressBar);

        nicknameTxtV = (TextView)findViewById(R.id.nicknameTxt);
        mostkillsTxtV = (TextView)findViewById(R.id.playerDmg);
        kdrTxtV = (TextView)findViewById(R.id.playerKills);
        winsTxtV = (TextView)findViewById(R.id.playerMostWins);
        header = (TextView)findViewById(R.id.headerTxt);
        winsLabel = (TextView)findViewById(R.id.winsLabel);
        kdrLabel = (TextView)findViewById(R.id.killsLabel);
        mostkillsLabel = (TextView)findViewById(R.id.damageLabel);

        icon1 = (ImageView)findViewById(R.id.icon1);
        icon2 = (ImageView)findViewById(R.id.icon2);
        icon3 = (ImageView)findViewById(R.id.icon3);

        nickname = (EditText)findViewById(R.id.nickname);
        avatarImg = (ImageView)findViewById(R.id.avatarImg);
        searchBtn = (Button)findViewById(R.id.searchbtn);
        viewProfileBtn = (Button)findViewById(R.id.profileBtn);


        profileIntent = new Intent(MainActivity.this, PlayerDataActivity.class);

        criteria = new FilterCriteria();

        criteria.setMode(PUBGMode.squad_fpp);
        criteria.setSeason(PUBGSeason.PRE5_2017);
        criteria.setRegion(PUBGRegion.eu);

        applySettings();

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new LoadPlayerDataTask().execute();
            }
        });



        viewProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profileIntent.putExtra("name", player.getPlayerName());
                startActivity(profileIntent);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.main_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            /*case R.id.friends:
                Intent friendsIntent = new Intent(MainActivity.this, FriendsActivity.class);
                startActivity(friendsIntent);
                return true;*/
            case R.id.settings:
                Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(settingsIntent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


    }

    private void applySettings(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        preferences.registerOnSharedPreferenceChangeListener(this);



    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if(s.equals("pref_region")){
            switch (sharedPreferences.getString(s, "EU")){
                case "EU":
                    criteria.setRegion(PUBGRegion.eu);
                    break;
                case "SEA":
                    criteria.setRegion(PUBGRegion.sea);
                    break;
                case "SA":
                    criteria.setRegion(PUBGRegion.sa);
                    break;
                case "NA":
                    criteria.setRegion(PUBGRegion.na);
                    break;
                case "KR/JP":
                    criteria.setRegion(PUBGRegion.krjp);
                    break;
                case "OC":
                    criteria.setRegion(PUBGRegion.oc);
                    break;
                case "AS":
                    criteria.setRegion(PUBGRegion.as);
                    break;
                default:
                    criteria.setRegion(PUBGRegion.eu);
            }
        }
        else if(s.equals("pref_gamemodes")){
            switch (sharedPreferences.getString(s, "Squad-Fpp")){
                case "Solo":
                    criteria.setMode(PUBGMode.solo);
                    break;
                case "Duo":
                    criteria.setMode(PUBGMode.duo);
                    break;
                case "Squad":
                    criteria.setMode(PUBGMode.squad);
                    break;
                case "Solo-Fpp":
                    criteria.setMode(PUBGMode.solo_fpp);
                    break;
                case "Duo-Fpp":
                    criteria.setMode(PUBGMode.duo_fpp);
                    break;
                case "Squad-Fpp":
                    criteria.setMode(PUBGMode.squad_fpp);
                    break;
                default:
                    criteria.setMode(PUBGMode.squad_fpp);
            }
        }
    }


    class LoadPlayerDataTask extends AsyncTask<Void,Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {


            try{

              player = jPubg.getByNickname(nickname.getText().toString(), criteria);

            }catch (Exception e){
                Toast.makeText(MainActivity.this, "Sorry, something went wrong", Toast.LENGTH_SHORT).show();

            }
            return null;
        }

       @Override
       protected void onPreExecute() {
           pb.setVisibility(View.VISIBLE);
       }

       @Override
        protected void onPostExecute(Void aVoid) {
           revealUiElements();
            try{

              Stat statMostKills = jPubg.getPlayerMatchStatByStatName(player, PUBGStat.ROUND_MOST_KILLS);
              Stat kdr = jPubg.getPlayerMatchStatByStatName(player, PUBGStat.KILL_DEATH_RATIO);

              Stat statWins = jPubg.getPlayerMatchStatByStatName(player, PUBGStat.WINS);

               nicknameTxtV.setText(player.getPlayerName());
               mostkillsTxtV.setText(statMostKills.getDisplayValue());
               kdrTxtV.setText(kdr.getDisplayValue());
               winsTxtV.setText(statWins.getDisplayValue());

               if(player.getAvatar() != null && !player.getAvatar().isEmpty()){
                   Glide.with(MainActivity.this).load(player.getAvatar()).into(avatarImg);
               }
            }
            catch (Exception e){
                Toast.makeText(MainActivity.this, "Couldn't retrieve the data. Please try again later " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }



    }

    private void revealUiElements(){
        pb.setVisibility(View.INVISIBLE);
        viewProfileBtn.setVisibility(View.VISIBLE);

        header.setVisibility(View.GONE);
        nicknameTxtV.setVisibility(View.VISIBLE);
        mostkillsTxtV.setVisibility(View.VISIBLE);
        winsTxtV.setVisibility(View.VISIBLE);
        avatarImg.setVisibility(View.VISIBLE);
        mostkillsLabel.setVisibility(View.VISIBLE);
        winsLabel.setVisibility(View.VISIBLE);
        kdrLabel.setVisibility(View.VISIBLE);
        icon1.setVisibility(View.VISIBLE);
        icon2.setVisibility(View.VISIBLE);
        icon3.setVisibility(View.VISIBLE);

    }
}
